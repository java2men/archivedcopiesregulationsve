package ru.admomsk.uikt.view;

import java.util.ArrayList;
import java.util.Date;

import ru.admomsk.uikt.app.ArchCopRegApplication;
import ru.admomsk.uikt.types.Individual;
import ru.admomsk.uikt.types.Legal;
import ru.admomsk.uikt.types.RequestContent;
import ru.admomsk.uikt.utils.DateUtils;
import ru.admomsk.uikt.utils.MyRefresher;
import ru.admomsk.uikt.utils.StringUtils;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Field;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

@SuppressWarnings("serial")
public class View extends Window {
	private CustomLayout viewLayout;
	private TextField tfPurposeRequest;
	private Label valPurposeRequest;
	private OptionGroup ogRequestedDocument;
	private Label valOgRequestedDocument;
	private CustomLayout culScopeDocument;
	private OptionGroup ogScopeDocument;
	private Label valOgScopeDocument;
	private TextField tfNameDocument;
	private Label valTfNameDocument;
	private TextField tfNumberDocument = null;
	private OptionGroup ogDatePublicDocument;
	private CustomLayout culExactDate;
	private PopupDateField pdfExactDate;
	private CustomLayout culDateRange;
	private PopupDateField pdfDateRange1;
	private PopupDateField pdfDateRange2;
	private TextField tfOtherInformation = null;
	private OptionGroup ogApplicant;
	private Label valOgApplicant;
	
	private CustomLayout culIndividual;
	private TextField tfIndividualLastName;
	private Label valTfIndividualLastName;
	private TextField tfIndividualFirstName;
	private Label valTfIndividualFirstName;
	private TextField tfIndividualMiddleName;
	//private Label valTfIndividualMiddleName;
	private CheckBox cbIndividualChangeLastName;
	private CustomLayout culIndividualChangeLastName;
	private TextField tfIndividualChangeLastName;
	private TextField tfIndividualSeriesPassport;
	private Label valTfIndividualSeriesPassport;
	private TextField tfIndividualNumberPassport;
	private Label valTfIndividualNumberPassport;
	private TextField tfIndividualWhoGivePassport;
	private Label valTfIndividualWhoGivePassport;
	private PopupDateField pdfIndividualDateIssuePassport;
	private Label valPdfIndividualDateIssuePassport;
	
	private TextField tfIndividualCountry;
	private Label valTfIndividualCountry;
	private TextField tfIndividualRegion;
	/*private Label valTfIndividualRegion;*/
	private TextField tfIndividualCity;
	private Label valTfIndividualCity;
	
	private TextField tfIndividualStreet;
	private Label valTfIndividualStreet;
	private TextField tfIndividualHome;
	private Label valTfIndividualHome;
	private TextField tfIndividualHousing;
	//private Label valTfIndividualHousing;
	private TextField tfIndividualApartment;
	//private Label valTfIndividualApartment;
	private TextField tfIndividualNumberTelephone;
	private Label valTfIndividualNumberTelephone;
	private TextField tfIndividualEmail;
	private Label valTfIndividualEmail;
	
	private CheckBox chPowerAttorney;
	private CheckBox chDocChangeName;
	private CheckBox chBirthCertificate;
	private CheckBox chDeathCertificate;
	private CheckBox chIndividualOtherDoc;
	private TextField tfIndividualOtherDoc;
	
	private CustomLayout culLegal;
	private TextField tfLegalFullName;
	private Label valTfLegalFullName;
	private TextField tfLegalBIN;
	private Label valTfLegalBIN;
	private TextField tfLegalINN;
	private Label valTfLegalINN;
	private TextField tfLegalLastName;
	private Label valTfLegalLastName;
	private TextField tfLegalFirstName;
	private Label valTfLegalFirstName;
	private TextField tfLegalMiddleName;
	//private Label valTfLegalMiddleName;
	private TextField tfLegalPostHead;
	private Label valTfLegalPostHead;
	
	private TextField tfLegalCountry;
	private Label valTfLegalCountry;
	private TextField tfLegalRegion;
	//private Label valTfLegalRegion;
	private TextField tfLegalCity;
	private Label valTfLegalCity;
	private TextField tfLegalStreet;
	private Label valTfLegalStreet;
	private TextField tfLegalHome;
	private Label valTfLegalHome;
	private TextField tfLegalHousing;
	//private Label valTfLegalHousing;
	private TextField tfLegalApartment;
	//private Label valTfLegalApartment;
	/*private TextField tfLegalMailAddress;
	private Label valTfLegalMailAddress;*/
	private TextField tfLegalNumberTelephone;
	private Label valTfLegalNumberTelephone;
	private TextField tfLegalEmail;
	private Label valTfLegalEmail;
	
	private TextField tfLegalOtherDoc = new TextField();
	
	private Button bSend;
	
	private CustomLayout culError;
	private CustomLayout culSent;
	
	private static String fieldRequired = "Поле обязательно для заполнения.";
	private static String incorrectType = "Введенное значение не является датой.";
	private static String emailValidate = "Адрес электронной почты введен некорректно.";
	private static String ogSelectValidate = "Не выбрано значение.";
	private FieldRequiredBlurListener fieldRequiredBlurListener;
	private PopupDataFieldRequiredBlurListener popupDataFieldRequiredBlurListener;
	private EmailValidateBlurListener emailValidateBlurListener;
	private OGValueChangeListener ogValueChangeListener;
	
	private RequestContent requestContent;
	private Individual individual;
	private Legal legal;
	
	private int refreshIntervalSession = 1000*5*60;
	
	private MyRefresher myRefresher = new MyRefresher();
	private MyRefresher sessionRefresher = new MyRefresher();
	
	public void setRefreshIntervalSession(int sInterval) {
		refreshIntervalSession = sInterval * 1000;
		sessionRefresher.setRefreshInterval(refreshIntervalSession);
	}
	
	public int getRefreshIntervalSession() {
		return refreshIntervalSession/1000;
	}
	
	public void startRefreshSession() {
		sessionRefresher.startRefresh();
	}
	
	public void stopRefreshSession() {
		sessionRefresher.stopRefresh();
	}
	
	public View() {
		viewLayout = buildViewLayout();
		//setContent(viewLayout);
		addComponent(viewLayout);
		
		//Слой для отображения успешной отправки
		culSent = buildSentLayout();
		culSent.setVisible(false);
		addComponent(culSent);
		
		//MyRefresher sessionRefresher = new MyRefresher();
		addComponent(sessionRefresher.create());
		sessionRefresher.setRefreshInterval(refreshIntervalSession);
	}
	
	
	//Слушатель для валидации пустого поля
	class FieldRequiredBlurListener implements BlurListener {
		private Field field;
		private Label label;
		
		public FieldRequiredBlurListener(Field field, Label label) {
			this.field = field;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			if (field.getValue() == null || field.getValue().toString().equals("")) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				field.addStyleName("lightError");
			}
			else {
				label.setValue(null);
				label.setVisible(false);
				field.removeStyleName("lightError");
			}
			
		}
		
	}
	
	//Слушатель для валидации пустого поля
	class OGValueChangeListener implements ValueChangeListener {
		private OptionGroup og;
		private Label label;
		
		public OGValueChangeListener(OptionGroup og, Label label) {
			this.og = og;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void valueChange(ValueChangeEvent event) {
			if (this.label.isVisible()) this.label.setVisible(false);
			this.label.setValue(null);
		}
		
	}
	
	//Слушатель для валидации Email
	class EmailValidateBlurListener implements BlurListener {
		private Field field;
		private Label label;
		
		public EmailValidateBlurListener(Field field, Label label) {
			this.field = field;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			if (field.getValue().toString().length()==0) return;
			if (!StringUtils.doMatch((String)field.getValue(), StringUtils.EMAIL)) {
				label.setValue(emailValidate);
				label.setVisible(true);
				field.addStyleName("lightError");
			}
			else {
				label.setValue(null);
				label.setVisible(false);
				field.removeStyleName("lightError");
			}
			
		}
		
	}
	
	//Слушатель
	class PopupDataFieldRequiredBlurListener implements BlurListener {
		private PopupDateField pdf;
		private Label label;
		
		public PopupDataFieldRequiredBlurListener(PopupDateField pdf, Label label) {
			this.pdf = pdf;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		
		
		public void blur(BlurEvent event) {
			
			//System.out.println("event.getSource()="+event.getSource().toString());
			//System.out.println("pdf.isValid()="+pdf.isValid());
			
			if ((pdf.getValue() == null && pdf.isValid()) /*|| pdf.getValue().toString().equals("")*/) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				pdf.addStyleName("lightError");
			} else {
				if (pdf.isValid()) {
					label.setValue(null);
					label.setVisible(false);
					pdf.removeStyleName("lightError");
				} else {
					label.setValue(incorrectType);
					label.setVisible(true);
					pdf.addStyleName("lightError");
				}
				
			}
			
		}
		
	}
	
	//Слушатель
	class PopupDataFieldRequiredValueChangeListener implements ValueChangeListener {
		private PopupDateField pdf;
		private Label label;
		
		public PopupDataFieldRequiredValueChangeListener(PopupDateField pdf, Label label) {
			this.pdf = pdf;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void valueChange(ValueChangeEvent event) {
			Object value = event.getProperty().getValue();
	        //System.out.println("value.toString()="+value.toString());
			if (value == null || value.toString().equals("")) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				pdf.addStyleName("lightError");
			} else {
				if (value instanceof Date) {
					label.setValue(null);
					label.setVisible(false);
					pdf.removeStyleName("lightError");
				} else {
					label.setValue(incorrectType);
					label.setVisible(true);
					pdf.addStyleName("lightError");
				}
				
			}
			
		}
		
	}
	
	//Слушатель
	class PopupDataIncorrectTypeBlurListener implements BlurListener {
		private PopupDateField pdf;
		private Label label;
		
		public PopupDataIncorrectTypeBlurListener(PopupDateField pdf, Label label) {
			this.pdf = pdf;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			
			if (pdf.getValue() == null || pdf.getValue().toString().equals("")) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				pdf.addStyleName("lightError");
			}
			else {
				label.setValue(null);
				label.setVisible(false);
				pdf.removeStyleName("lightError");
			}
			
		}
		
	}
	/*BlurListener fieldRequiredBlurListener = new BlurListener() {
		
		@Override
		public void blur(BlurEvent event) {
			//System.out.println(tfPurposeRequest.getValue());
			//System.out.println(event.getSource());
			//System.out.println(event.getSource().getClass());
			System.out.println(event.getComponent());
			event.get
			TextField textField = null;
			if (event.getComponent().getClass().toString().equals("TextField")){
				System.out.println("был тут");
				textField =(TextField)event.getComponent(); 
			}
			valFieldRequired.setContentMode(Label.CONTENT_XHTML);
			if (textField.getValue() == null ) {
				if (textField.getValue().toString().equals("")) {
					valFieldRequired.setValue(fieldRequired);
				}
			}
			else valFieldRequired.setValue(null);
		}
	};*/
	
	private CustomLayout buildViewLayout() {
		viewLayout = new CustomLayout("view");
		viewLayout.addStyleName("viewLayout");
		viewLayout.setImmediate(true);
		tfPurposeRequest = new TextField();
		tfPurposeRequest.addStyleName("tfPurposeRequest");
		tfPurposeRequest.setWordwrap(true);
		tfPurposeRequest.setRows(2);
		tfPurposeRequest.setImmediate(true);
		//Валидатор
		valPurposeRequest = new Label("",Label.CONTENT_XHTML);
		valPurposeRequest.addStyleName("valPurposeRequest");
		valPurposeRequest.addStyleName("validator");
		valPurposeRequest.setVisible(false);
		viewLayout.addComponent(valPurposeRequest, "valPurposeRequest");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfPurposeRequest, valPurposeRequest);
		tfPurposeRequest.addListener(fieldRequiredBlurListener);
		viewLayout.addComponent(tfPurposeRequest, "tfPurposeRequest");
		/*
		OptionGroup ogTest1 = new OptionGroup();
		ogTest1.setItemCaptionMode(OptionGroup.ITEM_CAPTION_MODE_ICON_ONLY);
		Label lTest1 = new Label("Test1", Label.CONTENT_XHTML);
		viewLayout.addComponent(ogTest1, "ogTest1");
		viewLayout.addComponent(lTest1, "lTest1");
		
		OptionGroup ogTest2 = new OptionGroup();
		ogTest2.setItemCaptionMode(OptionGroup.ITEM_CAPTION_MODE_ICON_ONLY);
		Label lTest2 = new Label("Test2", Label.CONTENT_XHTML);
		viewLayout.addComponent(ogTest2, "ogTest2");
		viewLayout.addComponent(lTest2, "lTest2");
		
		ControlOptionGroup cog = new ControlOptionGroup();
		cog.addValue(ogTest1, lTest1);
		cog.addValue(ogTest2, lTest2);
		*/
		ogRequestedDocument = new OptionGroup();
		ogRequestedDocument.addStyleName("ogRequestedDocument");
		//ogRequestedDocument.setItemCaptionMode(OptionGroup.ITEM_CAPTION_MODE_ICON_ONLY);
		ogRequestedDocument.setImmediate(true);
		Integer item = new Integer(1);
		ogRequestedDocument.addItem(item);
		ogRequestedDocument.setItemCaption(item, "копия постановления, распоряжения Администрации города Омска (Мэра города Омска) сроком издания не ранее 2006 года;");
		item = new Integer(2);
		ogRequestedDocument.addItem(item);
		ogRequestedDocument.setItemCaption(item, "копия ранее принятого распоряжения департамента, затрагивающего интересы граждан;");
		item = new Integer(3);
		ogRequestedDocument.addItem(item);
		ogRequestedDocument.setItemCaption(item, "копия архивного документа, подтверждающего право на владение землей.");
		//Валидатор
		valOgRequestedDocument = new Label("",Label.CONTENT_XHTML);
		valOgRequestedDocument.addStyleName("valOgRequestedDocument");
		valOgRequestedDocument.addStyleName("validator");
		valOgRequestedDocument.setVisible(false);
		viewLayout.addComponent(valOgRequestedDocument, "valOgRequestedDocument");
		
		ogRequestedDocument.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (valOgRequestedDocument.isVisible()) valOgRequestedDocument.setVisible(false);
				valOgRequestedDocument.setValue(null);
				
				if (event.getProperty().getValue().equals(2)) culScopeDocument.setVisible(true);
				else culScopeDocument.setVisible(false);
				}
			
			}
		);
		//ogValueChangeListener = new OGValueChangeListener(ogRequestedDocument, valOgRequestedDocument);
		//ogRequestedDocument.addListener(ogValueChangeListener);
		viewLayout.addComponent(ogRequestedDocument, "ogRequestedDocument");
		
		culScopeDocument = new CustomLayout("scope_document");
		culScopeDocument.addStyleName("culScopeDocument");
		culScopeDocument.setVisible(false);
		ogScopeDocument = new OptionGroup();
		ogScopeDocument.addStyleName("ogScopeDocument");
		ogScopeDocument.setImmediate(true);
		ogScopeDocument.addItem(new Integer(1));
		ogScopeDocument.setItemCaption(new Integer(1),"имущественные отношения;");
		ogScopeDocument.addItem(new Integer(2));
		ogScopeDocument.setItemCaption(new Integer(2),"образование;");
		ogScopeDocument.addItem(new Integer(3));
		ogScopeDocument.setItemCaption(new Integer(3),"архитектура и градостроительство.");
		//ogScopeDocument.addStyleName("option-group-scope-document");
		culScopeDocument.addComponent(ogScopeDocument, "ogScopeDocument");
		//Валидатор
		valOgScopeDocument = new Label("",Label.CONTENT_XHTML);
		valOgScopeDocument.addStyleName("valOgScopeDocument");
		valOgScopeDocument.addStyleName("validator");
		valOgScopeDocument.setVisible(false);
		culScopeDocument.addComponent(valOgScopeDocument, "valOgScopeDocument");
		//ogValueChangeListener = new OGValueChangeListener(ogScopeDocument, valOgScopeDocument);
		ogScopeDocument.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (valOgScopeDocument.isVisible()) valOgScopeDocument.setVisible(false);
				valOgScopeDocument.setValue(null);
			}
		});
		viewLayout.addComponent(culScopeDocument, "culScopeDocument");
		
		tfNameDocument = new TextField();
		tfNameDocument.setWordwrap(true);
		tfNameDocument.setRows(2);
		tfNameDocument.addStyleName("tfNameDocument");
		//Валидатор
		valTfNameDocument = new Label("",Label.CONTENT_XHTML);
		valTfNameDocument.addStyleName("valTfNameDocument");
		valTfNameDocument.addStyleName("validator");
		valTfNameDocument.setVisible(false);
		viewLayout.addComponent(valTfNameDocument, "valTfNameDocument");
		
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfNameDocument, valTfNameDocument);
		tfNameDocument.addListener(fieldRequiredBlurListener);
		viewLayout.addComponent(tfNameDocument, "tfNameDocument");
		
		tfNumberDocument = new TextField();
		tfNumberDocument.addStyleName("tfNumberDocument");
		viewLayout.addComponent(tfNumberDocument, "tfNumberDocument");
		
		ogDatePublicDocument = new OptionGroup();
		ogDatePublicDocument.addStyleName("ogDatePublicDocument");
		ogDatePublicDocument.setImmediate(true);
		item = new Integer(1);
		ogDatePublicDocument.addItem(item);
		ogDatePublicDocument.setItemCaption(item, "точная дата;");
		item = new Integer(2);
		ogDatePublicDocument.addItem(item);
		ogDatePublicDocument.setItemCaption(item, "диапазон дат. ");
		ogDatePublicDocument.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue().equals(1)) {
					culExactDate.setVisible(true);
					culDateRange.setVisible(false);
				}
				else {
					culExactDate.setVisible(false);
					culDateRange.setVisible(true);
				}
				
				
			}
		});
		viewLayout.addComponent(ogDatePublicDocument, "ogDatePublicDocument");
		
		culExactDate = new CustomLayout("exact_date");
		culExactDate.addStyleName("culExactDate");
		culExactDate.setVisible(false);
		pdfExactDate = new PopupDateField();
		//pdfExactDate.setEnabled(false);
		pdfExactDate.setInvalidAllowed(false);
		pdfExactDate.setValidationVisible(false);
		pdfExactDate.addStyleName("pdfExactDate");
		pdfExactDate.setResolution(4);
		//pdfExactDate.addStyleName("popup-date-field-exact-date");
		culExactDate.addComponent(pdfExactDate, "pdfExactDate");
		viewLayout.addComponent(culExactDate, "culExactDate");
		
		culDateRange = new CustomLayout("date_range");
		culDateRange.addStyleName("culDateRange");
		culDateRange.setVisible(false);
		pdfDateRange1 = new PopupDateField();
		pdfDateRange1.setValidationVisible(false);
		pdfDateRange1.addStyleName("pdfDateRange1");
		pdfDateRange1.setResolution(4);
		//pdfDateRange1.addStyleName("pdfDateRange1");
		culDateRange.addComponent(pdfDateRange1, "pdfDateRange1");
		pdfDateRange2 = new PopupDateField();
		pdfDateRange2.setValidationVisible(false);
		pdfDateRange2.addStyleName("pdfDateRange2");
		pdfDateRange2.setResolution(4);
		//pdfDateRange2.addStyleName("pdfDateRange2");
		culDateRange.addComponent(pdfDateRange2, "pdfDateRange2");
		viewLayout.addComponent(culDateRange, "culDateRange");
		
		tfOtherInformation = new TextField();
		tfOtherInformation.setWordwrap(true);
		tfOtherInformation.setRows(2);
		tfOtherInformation.addStyleName("tfOtherInformation");
		viewLayout.addComponent(tfOtherInformation, "tfOtherInformation");
		
		ogApplicant = new OptionGroup();
		ogApplicant.addStyleName("ogApplicant");
		ogApplicant.setImmediate(true);
		ogApplicant.addStyleName("option-group-applicant");
		item = new Integer(1);
		ogApplicant.addItem(item);
		ogApplicant.setItemCaption(item, "физическое лицо;");
		item = new Integer(2);
		ogApplicant.addItem(item);
		ogApplicant.setItemCaption(item, "юридическое лицо.");
		//Валидатор
		valOgApplicant = new Label("",Label.CONTENT_XHTML);
		valOgApplicant.addStyleName("valOgApplicant");
		valOgApplicant.addStyleName("validator");
		valOgApplicant.setVisible(false);
		viewLayout.addComponent(valOgApplicant, "valOgApplicant");
		ogApplicant.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				
				if (valOgApplicant.isVisible()) valOgApplicant.setVisible(false);
				valOgApplicant.setValue(null);
				
				if (event.getProperty().getValue().equals(1)) {
					culIndividual.setVisible(true);
					culLegal.setVisible(false);
				}
				else {
					culIndividual.setVisible(false);
					culLegal.setVisible(true);
				}
			}
		});
		viewLayout.addComponent(ogApplicant, "ogApplicant");
		
		culIndividual = buildIndividualLayout();
		culIndividual.setVisible(false);
		viewLayout.addComponent(culIndividual, "culIndividual");
		
		culLegal = buildLegalLayout();
		culLegal.setVisible(false);
		viewLayout.addComponent(culLegal, "culLegal");
		
		bSend = new Button("Отправить");
		bSend.addStyleName("bSend");
		bSend.setImmediate(true);
		bSend.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				//bSend.getWindow().setImmediate(true);
				//bSend.getWindow().setEnabled(false);
				
				
				requestContent = new RequestContent();
				individual = new Individual();
				legal = new Legal();
				boolean check = true;
				
				//Если цель запроса пуста
				if (tfPurposeRequest.getValue().toString().length()==0) {
					valPurposeRequest.setVisible(true);
					valPurposeRequest.setValue(fieldRequired);
					tfPurposeRequest.addStyleName("lightError");
					check = false;
				}
				else {
					requestContent.setPurposeRequest(tfPurposeRequest.getValue().toString());
					if (check) check = true;
				}
				
				//Если группа радиобутонов "Запрашиваемый документ" не выбранны
				if (ogRequestedDocument.getValue() == null) {
					valOgRequestedDocument.setVisible(true);
					valOgRequestedDocument.setValue(ogSelectValidate);
					check = false;
				}
				else {
					//Учесть regex шаблон для точки
					String strRequestedDocument = ogRequestedDocument.getItemCaption(ogRequestedDocument.getValue()).replaceAll(";", "").replaceAll("\\.", "");
					//strRequestedDocument = strRequestedDocument.replaceAll(".", "");
					requestContent.setRequestedDocument(strRequestedDocument);
					if (check) check = true;
				}
				
				/*Если группа радиобутонов "Сфера документа" не выбранны,
				при этом должен быть выбран запрашиваемый документ*/
				if (ogRequestedDocument.getValue() != null) {
					/*Включать валидатор Сферы документа, если Запрашиваемый документ установлен в позиции 2*/
					if (ogRequestedDocument.getValue().equals(2)) {
						if (ogScopeDocument.getValue() == null) {
							valOgScopeDocument.setVisible(true);
							valOgScopeDocument.setValue(ogSelectValidate);
							check = false;
						}
						else {
							String strScopeDocument = ogScopeDocument.getItemCaption(ogScopeDocument.getValue()).replaceAll(";", "").replaceAll("\\.", "");
							//strScopeDocument = strScopeDocument.replaceAll(".", "");
							requestContent.setScopeDocument(strScopeDocument);
							if (check) check = true;
						}
					} 
					
				}
				
				//Если Название документа пуста
				if (tfNameDocument.getValue().toString().length()==0) {
					valTfNameDocument.setVisible(true);
					valTfNameDocument.setValue(fieldRequired);
					tfNameDocument.addStyleName("lightError");
					check = false;
				}
				else {
					requestContent.setNameDocument(tfNameDocument.getValue().toString());
					if (check) check = true;
				}
				
				//Установить Номер документа
				if (tfNumberDocument.getValue() != null) requestContent.setNumberDocument(tfNumberDocument.getValue().toString());
				
				//Если выбрана Дата издания документа
				if (ogDatePublicDocument.getValue() != null) {
					
					if (ogDatePublicDocument.getValue().equals(1)) {
						//если не заполенно поле даты
						if (pdfExactDate.getValue() == null) {
							requestContent.setPublicationDate("");
						}
						else {
							DateUtils exactDate = new DateUtils((Date)pdfExactDate.getValue());
							requestContent.setPublicationDate(
									exactDate.getStringDay()+"."
								+exactDate.getStringMonth()+"."
								+exactDate.getStringYear()
							);
						}
					} else {
						//если не заполенно какое - либо поле диапозона дат
						if (pdfDateRange1.getValue() == null || pdfDateRange2.getValue() == null) {
							requestContent.setPublicationDate("");
						}
						else {
							DateUtils range1 = new DateUtils((Date)pdfDateRange1.getValue());
							DateUtils range2 = new DateUtils((Date)pdfDateRange2.getValue());
							requestContent.setPublicationDate(
								range1.getStringDay()+"."
								+range1.getStringMonth()+"."
								+range1.getStringYear()
								+" - "+
								range2.getStringDay()+"."
								+range2.getStringMonth()+"."
								+range2.getStringYear()
							);
						}
						
					}
				}
				
				/*System.out.println("tfPurposeRequest.getValue()="+tfPurposeRequest.getValue());
				System.out.println("tfNumberDocument.getValue()="+tfNumberDocument.getValue());
				System.out.println("tfOtherInformation.getValue()="+tfOtherInformation.getValue());
				*/
				//Установить Иные сведения
				if (tfOtherInformation.getValue() != null) requestContent.setOtherInformation(tfOtherInformation.getValue().toString());
				
				//Если группа радиобутонов "Заявитель" не выбранны
				if (ogApplicant.getValue() == null) {
					valOgApplicant.setVisible(true);
					valOgApplicant.setValue(ogSelectValidate);
					check = false;
				}else {
				
					//Если выбранно физ. лицо
					if (ogApplicant.getValue().equals(1)) {
						
						//Установить тип заявителя
						requestContent.setType("individual");
						
						//Если фамилия пуста
						if (tfIndividualLastName.getValue().toString().length()==0) {
							valTfIndividualLastName.setVisible(true);
							valTfIndividualLastName.setValue(fieldRequired);
							tfIndividualLastName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setLastName(tfIndividualLastName.getValue().toString());
							if (check) check = true;
						}
						
						//Если Имя пусто
						if (tfIndividualFirstName.getValue().toString().length()==0) {
							valTfIndividualFirstName.setVisible(true);
							valTfIndividualFirstName.setValue(fieldRequired);
							tfIndividualFirstName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setFirstName(tfIndividualFirstName.getValue().toString());
							if (check) check = true;
						}
						
						//Если Отчество пусто
						/*if (tfIndividualMiddleName.getValue().toString().length()==0) {
							valTfIndividualMiddleName.setVisible(true);
							valTfIndividualMiddleName.setValue(fieldRequired);
							tfIndividualMiddleName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setMiddleName(tfIndividualMiddleName.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualMiddleName.getValue() != null) individual.setMiddleName(tfIndividualMiddleName.getValue().toString());
						
						if (cbIndividualChangeLastName.booleanValue()) {
							individual.setChangeLastName(tfIndividualChangeLastName.getValue().toString());
						}
						
						//Если Серия паспорта пусто
						if (tfIndividualSeriesPassport.getValue().toString().length()==0) {
							valTfIndividualSeriesPassport.setVisible(true);
							valTfIndividualSeriesPassport.setValue(fieldRequired);
							tfIndividualSeriesPassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setSeriesPassport(tfIndividualSeriesPassport.getValue().toString());
							if (check) check = true;
						}
						
						//Если Номер паспорта пусто
						if (tfIndividualNumberPassport.getValue().toString().length()==0) {
							valTfIndividualNumberPassport.setVisible(true);
							valTfIndividualNumberPassport.setValue(fieldRequired);
							tfIndividualNumberPassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setNumberPassport(tfIndividualNumberPassport.getValue().toString());
							if (check) check = true;
						}
						
						//Если Кем выдан паспорт пусто
						if (tfIndividualWhoGivePassport.getValue().toString().length()==0) {
							valTfIndividualWhoGivePassport.setVisible(true);
							valTfIndividualWhoGivePassport.setValue(fieldRequired);
							tfIndividualWhoGivePassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setWhoGivePassport(tfIndividualWhoGivePassport.getValue().toString());
							if (check) check = true;
						}
						
						//Если Дата выдачи паспорта пусто
						if (pdfIndividualDateIssuePassport.getValue() == null) {
							valPdfIndividualDateIssuePassport.setVisible(true);
							if (pdfIndividualDateIssuePassport.isValid()) valPdfIndividualDateIssuePassport.setValue(fieldRequired);
							else valPdfIndividualDateIssuePassport.setValue(incorrectType);
							pdfIndividualDateIssuePassport.addStyleName("lightError");
							check = false;
						}
						else {
							DateUtils individualDateIssuePassport = new DateUtils((Date)pdfIndividualDateIssuePassport.getValue());
							individual.setDateIssuePassport(
								individualDateIssuePassport.getStringDay()+"."
								+individualDateIssuePassport.getStringMonth()+"."
								+individualDateIssuePassport.getStringYear()
							);
							if (check) check = true;
						}
						
						//Если Страна пусто
						if (tfIndividualCountry.getValue().toString().length()==0) {
							valTfIndividualCountry.setVisible(true);
							valTfIndividualCountry.setValue(fieldRequired);
							tfIndividualCountry.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setCountry(tfIndividualCountry.getValue().toString());
							if (check) check = true;
						}
						
						//Если Регион пусто
						/*
						if (tfIndividualRegion.getValue().toString().length()==0) {
							valTfIndividualRegion.setVisible(true);
							valTfIndividualRegion.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setRegion(tfIndividualRegion.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualRegion.getValue() != null) individual.setRegion(tfIndividualRegion.getValue().toString());
						
						//Если Город пусто
						if (tfIndividualCity.getValue().toString().length()==0) {
							valTfIndividualCity.setVisible(true);
							valTfIndividualCity.setValue(fieldRequired);
							tfIndividualCity.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setCity(tfIndividualCity.getValue().toString());
							if (check) check = true;
						}
						
						//Если Улица пусто
						if (tfIndividualStreet.getValue().toString().length()==0) {
							valTfIndividualStreet.setVisible(true);
							valTfIndividualStreet.setValue(fieldRequired);
							tfIndividualStreet.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setStreet(tfIndividualStreet.getValue().toString());
							if (check) check = true;
						}
						
						//Если Дом пусто
						if (tfIndividualHome.getValue().toString().length()==0) {
							valTfIndividualHome.setVisible(true);
							valTfIndividualHome.setValue(fieldRequired);
							tfIndividualHome.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setHome(tfIndividualHome.getValue().toString());
							if (check) check = true;
						}
						
						//Если Корпус пусто
						/*if (tfIndividualHousing.getValue().toString().length()==0) {
							valTfIndividualHousing.setVisible(true);
							valTfIndividualHousing.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setHousing(tfIndividualHousing.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualHousing.getValue() != null) individual.setHousing(tfIndividualHousing.getValue().toString());
						
						//Если Квартира пусто
						/*if (tfIndividualApartment.getValue().toString().length()==0) {
							valTfIndividualApartment.setVisible(true);
							valTfIndividualApartment.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setApartment(tfIndividualApartment.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualApartment.getValue() != null) individual.setApartment(tfIndividualApartment.getValue().toString());
						
						//Если Номер телефона пусто
						if (tfIndividualNumberTelephone.getValue().toString().length()==0) {
							valTfIndividualNumberTelephone.setVisible(true);
							valTfIndividualNumberTelephone.setValue(fieldRequired);
							tfIndividualNumberTelephone.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setNumberTelephone(tfIndividualNumberTelephone.getValue().toString());
							if (check) check = true;
						}
						
						//Если Адрес электронной почты пусто
						if (tfIndividualEmail.getValue().toString().length()==0) {
							valTfIndividualEmail.setVisible(true);
							valTfIndividualEmail.setValue(fieldRequired);
							tfIndividualEmail.addStyleName("lightError");
							check = false;
						}
						else {
							//Если Адрес электронной почты не валиден							
							if (!StringUtils.doMatch(tfIndividualEmail.getValue().toString(), StringUtils.EMAIL)) {
								valTfIndividualEmail.setVisible(true);
								valTfIndividualEmail.setValue(emailValidate);
								tfIndividualEmail.addStyleName("lightError");
								check = false;
							}
							else {
								individual.setEmail(tfIndividualEmail.getValue().toString());
								if (check) check = true;
							}
						}
						
						ArrayList<String> supportDoc = new ArrayList<String>();
						if (chPowerAttorney.booleanValue()) supportDoc.add(chPowerAttorney.getCaption().replaceAll(";", ""));
						if (chDocChangeName.booleanValue()) supportDoc.add(chDocChangeName.getCaption().replaceAll(";", ""));
						if (chBirthCertificate.booleanValue()) supportDoc.add(chBirthCertificate.getCaption().replaceAll(";", ""));
						if (chDeathCertificate.booleanValue()) supportDoc.add(chDeathCertificate.getCaption().replaceAll(";", ""));
						if (chIndividualOtherDoc.booleanValue()) {
							//Если непустое
							if (!tfIndividualOtherDoc.getValue().equals("")) {
								supportDoc.add(tfIndividualOtherDoc.getValue().toString());
							}
						}
						individual.setSupportDoc(supportDoc);
						
					}
					//Если выбранно юр. лицо
					else {
						
						//Установить тип заявителя
						requestContent.setType("legal");
						
						//Если Полное наименование пусто
						if (tfLegalFullName.getValue().toString().length()==0) {
							valTfLegalFullName.setVisible(true);
							valTfLegalFullName.setValue(fieldRequired);
							tfLegalFullName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setFullName(tfLegalFullName.getValue().toString());
							if (check) check = true;
						}
						
						//Если ОГРН пусто
						if (tfLegalBIN.getValue().toString().length()==0) {
							valTfLegalBIN.setVisible(true);
							valTfLegalBIN.setValue(fieldRequired);
							tfLegalBIN.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setBIN(tfLegalBIN.getValue().toString());
							if (check) check = true;
						}
						
						//Если ИНН пусто
						if (tfLegalINN.getValue().toString().length()==0) {
							valTfLegalINN.setVisible(true);
							valTfLegalINN.setValue(fieldRequired);
							tfLegalINN.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setINN(tfLegalINN.getValue().toString());
							if (check) check = true;
						}
						
						//Если Фамилия пуста
						if (tfLegalLastName.getValue().toString().length()==0) {
							valTfLegalLastName.setVisible(true);
							valTfLegalLastName.setValue(fieldRequired);
							tfLegalLastName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setLastName(tfLegalLastName.getValue().toString());
							if (check) check = true;
						}

						//Если Имя пусто
						if (tfLegalFirstName.getValue().toString().length()==0) {
							valTfLegalFirstName.setVisible(true);
							valTfLegalFirstName.setValue(fieldRequired);
							tfLegalFirstName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setFirstName(tfLegalFirstName.getValue().toString());
							if (check) check = true;
						}

						//Если Отчество пусто
						/*if (tfLegalMiddleName.getValue().toString().length()==0) {
							valTfLegalMiddleName.setVisible(true);
							valTfLegalMiddleName.setValue(fieldRequired);
							tfLegalMiddleName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setMiddleName(tfLegalMiddleName.getValue().toString());
							if (check) check = true;
						}*/
						if (tfLegalMiddleName.getValue() != null) legal.setMiddleName(tfLegalMiddleName.getValue().toString());
						
						//Если Должность руководителя пусто
						if (tfLegalPostHead.getValue().toString().length()==0) {
							valTfLegalPostHead.setVisible(true);
							valTfLegalPostHead.setValue(fieldRequired);
							tfLegalPostHead.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setPostHead(tfLegalPostHead.getValue().toString());
							if (check) check = true;
						}
						
						//Если Страна пусто
						if (tfLegalCountry.getValue().toString().length()==0) {
							valTfLegalCountry.setVisible(true);
							valTfLegalCountry.setValue(fieldRequired);
							tfLegalCountry.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setCountry(tfLegalCountry.getValue().toString());
							if (check) check = true;
						}

						//Если Регион пусто
						/*
						if (tfLegalRegion.getValue().toString().length()==0) {
							valTfLegalRegion.setVisible(true);
							valTfLegalRegion.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setRegion(tfLegalRegion.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfLegalRegion.getValue() != null) legal.setRegion(tfLegalRegion.getValue().toString());

						//Если Город пусто
						if (tfLegalCity.getValue().toString().length()==0) {
							valTfLegalCity.setVisible(true);
							valTfLegalCity.setValue(fieldRequired);
							tfLegalCity.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setCity(tfLegalCity.getValue().toString());
							if (check) check = true;
						}

						//Если Улица пусто
						if (tfLegalStreet.getValue().toString().length()==0) {
							valTfLegalStreet.setVisible(true);
							valTfLegalStreet.setValue(fieldRequired);
							tfLegalStreet.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setStreet(tfLegalStreet.getValue().toString());
							if (check) check = true;
						}

						//Если Дом пусто
						if (tfLegalHome.getValue().toString().length()==0) {
							valTfLegalHome.setVisible(true);
							valTfLegalHome.setValue(fieldRequired);
							tfLegalHome.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setHome(tfLegalHome.getValue().toString());
							if (check) check = true;
						}

						//Если Корпус пусто
						/*
						if (tfLegalHousing.getValue().toString().length()==0) {
							valTfLegalHousing.setVisible(true);
							valTfLegalHousing.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setHousing(tfLegalHousing.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfLegalHousing.getValue() != null) legal.setHousing(tfLegalHousing.getValue().toString());
						
						//Если Квартира пусто
						/*
						if (tfLegalApartment.getValue().toString().length()==0) {
							valTfLegalApartment.setVisible(true);
							valTfLegalApartment.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setApartment(tfLegalApartment.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfLegalApartment.getValue() != null) legal.setApartment(tfLegalApartment.getValue().toString());
						
						//Если Почтовый адрес пусто
						/*
						if (tfLegalMailAddress.getValue().toString().length()==0) {
							valTfLegalMailAddress.setVisible(true);
							valTfLegalMailAddress.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setMailAddress(tfLegalMailAddress.getValue().toString());
							if (check) check = true;
						}
						*/
						
						//Если Номер телефона пусто
						if (tfLegalNumberTelephone.getValue().toString().length()==0) {
							valTfLegalNumberTelephone.setVisible(true);
							valTfLegalNumberTelephone.setValue(fieldRequired);
							tfLegalNumberTelephone.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setNumberTelephone(tfLegalNumberTelephone.getValue().toString());
							if (check) check = true;
						}
						
						//Если Адрес электронной почты пусто
						if (tfLegalEmail.getValue().toString().length()==0) {
							valTfLegalEmail.setVisible(true);
							valTfLegalEmail.setValue(fieldRequired);
							tfLegalEmail.addStyleName("lightError");
							check = false;
						}
						else {
							//Если Адрес электронной почты не валиден							
							if (!StringUtils.doMatch(tfLegalEmail.getValue().toString(), StringUtils.EMAIL)) {
								valTfLegalEmail.setVisible(true);
								valTfLegalEmail.setValue(emailValidate);
								tfLegalEmail.addStyleName("lightError");
								check = false;
							}
							else {
								legal.setEmail(tfLegalEmail.getValue().toString());
								if (check) check = true;
							}
						}
						
						if (tfLegalOtherDoc.getValue() != null) legal.setSupportDoc(tfLegalOtherDoc.getValue().toString());
						
					}
				}
				
				//Отправить письмо
				//System.out.println("check="+check);
				//check=true;
				//если все данные заполенны
				if (check) {
					
					getWindow().addComponent(myRefresher.create());
					myRefresher.setRefreshInterval(1000);
					myRefresher.startRefresh();
					viewLayout.setEnabled(false);
					
					new Thread(new Runnable() {
						
						public void run() {
							/*если письмо отправлено, установить
							слой отображающий успешности отправки*/
							if (ArchCopRegApplication.sendMessage(requestContent, individual, legal)) {
								//setContent(culSent);
								viewLayout.setVisible(false);
								culSent.setVisible(true);
								//Разрешить пересоздать View, а-ля почистить сессию
								ArchCopRegApplication.reCreateView();
							}
							/*если письмо не отправлено, 
							установить сделать видимым слой отображающий ошибку отправки*/
							else {
								culError.setVisible(true);
							}
							
				            synchronized (getApplication()) {
				            	viewLayout.setEnabled(true);
				            }
				            myRefresher.stopRefresh();
						}
						
					}).start();
					
					
				}
				
			}
		});
		viewLayout.addComponent(bSend, "bSend");
		
		//Слой для отображения ошибки
		culError = buildErrorLayout();
		culError.setVisible(false);
		viewLayout.addComponent(culError, "culError");
		
		return viewLayout;
	}
	
	private CustomLayout buildIndividualLayout() {
		culIndividual = new CustomLayout("individual");
		culIndividual.addStyleName("culIndividual");
		
		tfIndividualLastName = new TextField();
		tfIndividualLastName.addStyleName("tfIndividualLastName");
		valTfIndividualLastName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualLastName.addStyleName("valTfIndividualLastName");
		valTfIndividualLastName.addStyleName("validator");
		valTfIndividualLastName.setVisible(true);
		culIndividual.addComponent(valTfIndividualLastName, "valTfIndividualLastName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualLastName, valTfIndividualLastName);
		tfIndividualLastName.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualLastName, "tfIndividualLastName");
		
		tfIndividualFirstName = new TextField();
		tfIndividualFirstName.addStyleName("tfIndividualFirstName");
		valTfIndividualFirstName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualFirstName.addStyleName("valTfIndividualFirstName");
		valTfIndividualFirstName.addStyleName("validator");
		valTfIndividualFirstName.setVisible(false);
		culIndividual.addComponent(valTfIndividualFirstName, "valTfIndividualFirstName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualFirstName, valTfIndividualFirstName);
		tfIndividualFirstName.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualFirstName, "tfIndividualFirstName");
		
		tfIndividualMiddleName = new TextField();
		tfIndividualMiddleName.addStyleName("tfIndividualMiddleName");
		/*valTfIndividualMiddleName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualMiddleName.addStyleName("valTfIndividualMiddleName");
		valTfIndividualMiddleName.addStyleName("validator");
		valTfIndividualMiddleName.setVisible(false);
		culIndividual.addComponent(valTfIndividualMiddleName, "valTfIndividualMiddleName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualMiddleName, valTfIndividualMiddleName);
		tfIndividualMiddleName.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualMiddleName, "tfIndividualMiddleName");
		
		cbIndividualChangeLastName = new CheckBox();
		cbIndividualChangeLastName.addStyleName("cbIndividualChangeLastName");
		cbIndividualChangeLastName.setImmediate(true);
		cbIndividualChangeLastName.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				culIndividualChangeLastName.setVisible(cbIndividualChangeLastName.booleanValue());
			}
		});
		culIndividual.addComponent(cbIndividualChangeLastName, "cbIndividualChangeLastName");
		
		culIndividualChangeLastName = new CustomLayout("change_lastname");
		culIndividualChangeLastName.setVisible(false);
		tfIndividualChangeLastName = new TextField();
		tfIndividualChangeLastName.addStyleName("tfIndividualChangeLastName");
		culIndividualChangeLastName.addComponent(tfIndividualChangeLastName, "tfIndividualChangeLastName");
		culIndividual.addComponent(culIndividualChangeLastName, "culIndividualChangeLastName");
		
		tfIndividualSeriesPassport = new TextField();
		tfIndividualSeriesPassport.addStyleName("tfIndividualSeriesPassport");
		valTfIndividualSeriesPassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualSeriesPassport.addStyleName("valTfIndividualSeriesPassport");
		valTfIndividualSeriesPassport.addStyleName("validator");
		valTfIndividualSeriesPassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualSeriesPassport, "valTfIndividualSeriesPassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualSeriesPassport, valTfIndividualSeriesPassport);
		tfIndividualSeriesPassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualSeriesPassport, "tfIndividualSeriesPassport");
		
		tfIndividualNumberPassport = new TextField();
		tfIndividualNumberPassport.addStyleName("tfIndividualNumberPassport");
		valTfIndividualNumberPassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualNumberPassport.addStyleName("valTfIndividualNumberPassport");
		valTfIndividualNumberPassport.addStyleName("validator");
		valTfIndividualNumberPassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualNumberPassport, "valTfIndividualNumberPassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualNumberPassport, valTfIndividualNumberPassport);
		tfIndividualNumberPassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualNumberPassport, "tfIndividualNumberPassport");
		
		tfIndividualWhoGivePassport = new TextField();
		tfIndividualWhoGivePassport.setWordwrap(true);
		tfIndividualWhoGivePassport.setRows(2);
		tfIndividualWhoGivePassport.addStyleName("tfIndividualWhoGivePassport");
		valTfIndividualWhoGivePassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualWhoGivePassport.addStyleName("valTfIndividualWhoGivePassport");
		valTfIndividualWhoGivePassport.addStyleName("validator");
		valTfIndividualWhoGivePassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualWhoGivePassport, "valTfIndividualWhoGivePassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualWhoGivePassport, valTfIndividualWhoGivePassport);
		tfIndividualWhoGivePassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualWhoGivePassport, "tfIndividualWhoGivePassport");
		
		pdfIndividualDateIssuePassport = new PopupDateField();
		pdfIndividualDateIssuePassport.setValidationVisible(false);
		pdfIndividualDateIssuePassport.addStyleName("pdfIndividualDateIssuePassport");
		pdfIndividualDateIssuePassport.setResolution(4);
		pdfIndividualDateIssuePassport.setRequired(false);
		valPdfIndividualDateIssuePassport = new Label("",Label.CONTENT_XHTML);
		valPdfIndividualDateIssuePassport.addStyleName("valPdfIndividualDateIssuePassport");
		valPdfIndividualDateIssuePassport.addStyleName("validator");
		valPdfIndividualDateIssuePassport.setVisible(false);
		culIndividual.addComponent(valPdfIndividualDateIssuePassport, "valPdfIndividualDateIssuePassport");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfIndividualDateIssuePassport, valPdfIndividualDateIssuePassport);
		pdfIndividualDateIssuePassport.addListener(popupDataFieldRequiredBlurListener);
		culIndividual.addComponent(pdfIndividualDateIssuePassport, "pdfIndividualDateIssuePassport");
		
		tfIndividualCountry = new TextField();
		tfIndividualCountry.setValue("Россия");
		tfIndividualCountry.addStyleName("tfIndividualCountry");
		valTfIndividualCountry = new Label("",Label.CONTENT_XHTML);
		valTfIndividualCountry.addStyleName("valTfIndividualCountry");
		valTfIndividualCountry.addStyleName("validator");
		valTfIndividualCountry.setVisible(false);
		culIndividual.addComponent(valTfIndividualCountry, "valTfIndividualCountry");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualCountry, valTfIndividualCountry);
		tfIndividualCountry.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualCountry, "tfIndividualCountry");
		
		tfIndividualRegion = new TextField();
		tfIndividualRegion.setValue("Омская область");
		tfIndividualRegion.addStyleName("tfIndividualRegion");
		/*
		valTfIndividualRegion = new Label("",Label.CONTENT_XHTML);
		valTfIndividualRegion.addStyleName("valTfIndividualRegion");
		valTfIndividualRegion.setVisible(false);
		culIndividual.addComponent(valTfIndividualRegion, "valTfIndividualRegion");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualRegion, valTfIndividualRegion);
		tfIndividualRegion.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualRegion, "tfIndividualRegion");
		
		tfIndividualCity = new TextField();
		tfIndividualCity.setValue("Омск");
		tfIndividualCity.addStyleName("tfIndividualCity");
		valTfIndividualCity = new Label("",Label.CONTENT_XHTML);
		valTfIndividualCity.addStyleName("valTfIndividualCity");
		valTfIndividualCity.addStyleName("validator");
		valTfIndividualCity.setVisible(false);
		culIndividual.addComponent(valTfIndividualCity, "valTfIndividualCity");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualCity, valTfIndividualCity);
		tfIndividualCity.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualCity, "tfIndividualCity");
		
		tfIndividualStreet = new TextField();
		tfIndividualStreet.addStyleName("tfIndividualStreet");
		valTfIndividualStreet = new Label("",Label.CONTENT_XHTML);
		valTfIndividualStreet.addStyleName("valTfIndividualStreet");
		valTfIndividualStreet.addStyleName("validator");
		valTfIndividualStreet.setVisible(false);
		culIndividual.addComponent(valTfIndividualStreet, "valTfIndividualStreet");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualStreet, valTfIndividualStreet);
		tfIndividualStreet.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualStreet, "tfIndividualStreet");
		
		tfIndividualHome = new TextField();
		tfIndividualHome.addStyleName("tfIndividualHome");
		valTfIndividualHome = new Label("",Label.CONTENT_XHTML);
		valTfIndividualHome.addStyleName("valTfIndividualHome");
		valTfIndividualHome.addStyleName("validator");
		valTfIndividualHome.setVisible(false);
		culIndividual.addComponent(valTfIndividualHome, "valTfIndividualHome");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualHome, valTfIndividualHome);
		tfIndividualHome.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualHome, "tfIndividualHome");
		
		tfIndividualHousing = new TextField();
		tfIndividualHousing.addStyleName("tfIndividualHousing");
		/*valTfIndividualHousing = new Label("",Label.CONTENT_XHTML);
		valTfIndividualHousing.addStyleName("valTfIndividualHousing");
		valTfIndividualHousing.setVisible(false);
		culIndividual.addComponent(valTfIndividualHousing, "valTfIndividualHousing");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualHousing, valTfIndividualHousing);
		tfIndividualHousing.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualHousing, "tfIndividualHousing");
		
		tfIndividualApartment = new TextField();
		tfIndividualApartment.addStyleName("tfIndividualApartment");
		/*
		valTfIndividualApartment = new Label("",Label.CONTENT_XHTML);
		valTfIndividualApartment.addStyleName("valTfIndividualApartment");
		valTfIndividualApartment.setVisible(false);
		culIndividual.addComponent(valTfIndividualApartment, "valTfIndividualApartment");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualApartment, valTfIndividualApartment);
		tfIndividualApartment.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualApartment, "tfIndividualApartment");
		
		tfIndividualNumberTelephone = new TextField();
		tfIndividualNumberTelephone.addStyleName("tfIndividualNumberTelephone");
		valTfIndividualNumberTelephone = new Label("",Label.CONTENT_XHTML);
		valTfIndividualNumberTelephone.addStyleName("valTfIndividualNumberTelephone");
		valTfIndividualNumberTelephone.addStyleName("validator");
		valTfIndividualNumberTelephone.setVisible(false);
		culIndividual.addComponent(valTfIndividualNumberTelephone, "valTfIndividualNumberTelephone");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualNumberTelephone, valTfIndividualNumberTelephone);
		tfIndividualNumberTelephone.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualNumberTelephone, "tfIndividualNumberTelephone");
		
		tfIndividualEmail = new TextField();
		tfIndividualEmail.addStyleName("tfIndividualEmail");
		valTfIndividualEmail = new Label("",Label.CONTENT_XHTML);
		valTfIndividualEmail.addStyleName("valTfIndividualEmail");
		valTfIndividualEmail.addStyleName("validator");
		valTfIndividualEmail.setVisible(false);
		culIndividual.addComponent(valTfIndividualEmail, "valTfIndividualEmail");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualEmail, valTfIndividualEmail);
		tfIndividualEmail.addListener(fieldRequiredBlurListener);
		emailValidateBlurListener = new EmailValidateBlurListener(tfIndividualEmail, valTfIndividualEmail);
		tfIndividualEmail.addListener(emailValidateBlurListener);
		culIndividual.addComponent(tfIndividualEmail, "tfIndividualEmail");
		
		chPowerAttorney = new CheckBox("доверенность;");
		chPowerAttorney.addStyleName("chPowerAttorney");
		//chPowerAttorney.setImmediate(true);
		culIndividual.addComponent(chPowerAttorney, "chPowerAttorney");
		
		chDocChangeName = new CheckBox("документ, подтверждающий смену фамилии, имени, отчества;");
		//chDocChangeName.setVisible(false);
		chDocChangeName.addStyleName("chDocChangeName");
		culIndividual.addComponent(chDocChangeName, "chDocChangeName");
		
		chBirthCertificate = new CheckBox("свидетельство о рождении;");
		//chBirthCertificate.setVisible(false);
		chBirthCertificate.addStyleName("chBirthCertificate");
		//chBirthCertificate.setImmediate(true);
		culIndividual.addComponent(chBirthCertificate, "chBirthCertificate");
		
		chDeathCertificate = new CheckBox("свидетельство о смерти;");
		//chDeathCertificate.setVisible(false);
		chDeathCertificate.addStyleName("chDeathCertificate");
		//chDeathCertificate.setImmediate(true);
		culIndividual.addComponent(chDeathCertificate, "chDeathCertificate");
		
		chIndividualOtherDoc = new CheckBox("иное:");
		//chIndividualOtherDoc.setVisible(false);
		chIndividualOtherDoc.addStyleName("chIndividualOtherDoc");
		chIndividualOtherDoc.setImmediate(true);
		culIndividual.addComponent(chIndividualOtherDoc, "chIndividualOtherDoc");
		
		chIndividualOtherDoc.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				tfIndividualOtherDoc.setVisible(chIndividualOtherDoc.booleanValue());
			}
		});
		
		tfIndividualOtherDoc = new TextField();
		tfIndividualOtherDoc.addStyleName("tfIndividualOtherDoc");
		tfIndividualOtherDoc.setVisible(false);
		tfIndividualOtherDoc.setWordwrap(true);
		tfIndividualOtherDoc.setRows(2);
		culIndividual.addComponent(tfIndividualOtherDoc, "tfIndividualOtherDoc");
		
		return culIndividual;
	}
	
	private CustomLayout buildLegalLayout() {
		culLegal = new CustomLayout("legal");
		culLegal.addStyleName("culLegal");
		
		tfLegalFullName = new TextField();
		tfLegalFullName.addStyleName("tfLegalFullName");
		valTfLegalFullName = new Label("",Label.CONTENT_XHTML);
		valTfLegalFullName.addStyleName("valTfLegalFullName");
		valTfLegalFullName.addStyleName("validator");
		valTfLegalFullName.setVisible(true);
		culLegal.addComponent(valTfLegalFullName, "valTfLegalFullName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalFullName, valTfLegalFullName);
		tfLegalFullName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalFullName, "tfLegalFullName");
		
		tfLegalBIN = new TextField();
		tfLegalBIN.addStyleName("tfLegalBIN");
		valTfLegalBIN = new Label("",Label.CONTENT_XHTML);
		valTfLegalBIN.addStyleName("valTfLegalBIN");
		valTfLegalBIN.addStyleName("validator");
		valTfLegalBIN.setVisible(true);
		culLegal.addComponent(valTfLegalBIN, "valTfLegalBIN");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalBIN, valTfLegalBIN);
		tfLegalBIN.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalBIN, "tfLegalBIN");
		
		tfLegalINN = new TextField();
		tfLegalINN.addStyleName("tfLegalINN");
		valTfLegalINN = new Label("",Label.CONTENT_XHTML);
		valTfLegalINN.addStyleName("valTfLegalINN");
		valTfLegalINN.addStyleName("validator");
		valTfLegalINN.setVisible(true);
		culLegal.addComponent(valTfLegalINN, "valTfLegalINN");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalINN, valTfLegalINN);
		tfLegalINN.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalINN, "tfLegalINN");
		
		tfLegalLastName = new TextField();
		tfLegalLastName.addStyleName("tfLegalLastName");
		valTfLegalLastName = new Label("",Label.CONTENT_XHTML);
		valTfLegalLastName.addStyleName("valTfLegalLastName");
		valTfLegalLastName.addStyleName("validator");
		valTfLegalLastName.setVisible(true);
		culLegal.addComponent(valTfLegalLastName, "valTfLegalLastName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalLastName, valTfLegalLastName);
		tfLegalLastName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalLastName, "tfLegalLastName");
		
		tfLegalFirstName = new TextField();
		tfLegalFirstName.addStyleName("tfLegalFirstName");
		valTfLegalFirstName = new Label("",Label.CONTENT_XHTML);
		valTfLegalFirstName.addStyleName("valTfLegalFirstName");
		valTfLegalFirstName.addStyleName("validator");
		valTfLegalFirstName.setVisible(true);
		culLegal.addComponent(valTfLegalFirstName, "valTfLegalFirstName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalFirstName, valTfLegalFirstName);
		tfLegalFirstName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalFirstName, "tfLegalFirstName");
		
		tfLegalMiddleName = new TextField();
		tfLegalMiddleName.addStyleName("tfLegalMiddleName");
		/*
		valTfLegalMiddleName = new Label("",Label.CONTENT_XHTML);
		valTfLegalMiddleName.addStyleName("valTfLegalMiddleName");
		valTfLegalMiddleName.addStyleName("validator");
		valTfLegalMiddleName.setVisible(true);
		culLegal.addComponent(valTfLegalMiddleName, "valTfLegalMiddleName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalMiddleName, valTfLegalMiddleName);
		tfLegalMiddleName.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalMiddleName, "tfLegalMiddleName");
		
		tfLegalPostHead = new TextField();
		tfLegalPostHead.addStyleName("tfLegalPostHead");
		valTfLegalPostHead = new Label("",Label.CONTENT_XHTML);
		valTfLegalPostHead.addStyleName("valTfLegalPostHead");
		valTfLegalPostHead.addStyleName("validator");
		valTfLegalPostHead.setVisible(true);
		culLegal.addComponent(valTfLegalPostHead, "valTfLegalPostHead");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalPostHead, valTfLegalPostHead);
		tfLegalPostHead.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalPostHead, "tfLegalPostHead");
		
		
		tfLegalCountry = new TextField();
		tfLegalCountry.setValue("Россия");
		tfLegalCountry.addStyleName("tfLegalCountry");
		valTfLegalCountry = new Label("",Label.CONTENT_XHTML);
		valTfLegalCountry.addStyleName("valTfLegalCountry");
		valTfLegalCountry.addStyleName("validator");
		valTfLegalCountry.setVisible(true);
		culLegal.addComponent(valTfLegalCountry, "valTfLegalCountry");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalCountry, valTfLegalCountry);
		tfLegalCountry.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalCountry, "tfLegalCountry");
		
		tfLegalRegion = new TextField();
		tfLegalRegion.setValue("Омская область");
		tfLegalRegion.addStyleName("tfLegalRegion");
		/*
		valTfLegalRegion = new Label("",Label.CONTENT_XHTML);
		valTfLegalRegion.addStyleName("valTfLegalRegion");
		valTfLegalRegion.setVisible(true);
		culLegal.addComponent(valTfLegalRegion, "valTfLegalRegion");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalRegion, valTfLegalRegion);
		tfLegalRegion.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalRegion, "tfLegalRegion");
		
		tfLegalCity = new TextField();
		tfLegalCity.setValue("Омск");
		tfLegalCity.addStyleName("tfLegalCity");
		valTfLegalCity = new Label("",Label.CONTENT_XHTML);
		valTfLegalCity.addStyleName("valTfLegalCity");
		valTfLegalCity.addStyleName("validator");
		valTfLegalCity.setVisible(true);
		culLegal.addComponent(valTfLegalCity, "valTfLegalCity");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalCity, valTfLegalCity);
		tfLegalCity.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalCity, "tfLegalCity");
		
		tfLegalStreet = new TextField();
		tfLegalStreet.addStyleName("tfLegalStreet");
		valTfLegalStreet = new Label("",Label.CONTENT_XHTML);
		valTfLegalStreet.addStyleName("valTfLegalStreet");
		valTfLegalStreet.addStyleName("validator");
		valTfLegalStreet.setVisible(true);
		culLegal.addComponent(valTfLegalStreet, "valTfLegalStreet");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalStreet, valTfLegalStreet);
		tfLegalStreet.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalStreet, "tfLegalStreet");
		
		tfLegalHome = new TextField();
		tfLegalHome.addStyleName("tfLegalHome");
		valTfLegalHome = new Label("",Label.CONTENT_XHTML);
		valTfLegalHome.addStyleName("valTfLegalHome");
		valTfLegalHome.addStyleName("validator");
		valTfLegalHome.setVisible(true);
		culLegal.addComponent(valTfLegalHome, "valTfLegalHome");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalHome, valTfLegalHome);
		tfLegalHome.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalHome, "tfLegalHome");
		
		tfLegalHousing = new TextField();
		tfLegalHousing.addStyleName("tfLegalHousing");
		/*
		valTfLegalHousing = new Label("",Label.CONTENT_XHTML);
		valTfLegalHousing.addStyleName("valTfLegalHousing");
		valTfLegalHousing.setVisible(true);
		culLegal.addComponent(valTfLegalHousing, "valTfLegalHousing");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalHousing, valTfLegalHousing);
		tfLegalHousing.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalHousing, "tfLegalHousing");
		
		tfLegalApartment = new TextField();
		tfLegalApartment.addStyleName("tfLegalApartment");
		/*
		valTfLegalApartment = new Label("",Label.CONTENT_XHTML);
		valTfLegalApartment.addStyleName("valTfLegalApartment");
		valTfLegalApartment.setVisible(true);
		culLegal.addComponent(valTfLegalApartment, "valTfLegalApartment");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalApartment, valTfLegalApartment);
		tfLegalApartment.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalApartment, "tfLegalApartment");
		
		/*
		tfLegalMailAddress = new TextField();
		tfLegalMailAddress.addStyleName("tfLegalMailAddress");
		valTfLegalMailAddress = new Label("",Label.CONTENT_XHTML);
		valTfLegalMailAddress.addStyleName("valTfLegalMailAddress");
		valTfLegalMailAddress.setVisible(true);
		culLegal.addComponent(valTfLegalMailAddress, "valTfLegalMailAddress");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalMailAddress, valTfLegalMailAddress);
		tfLegalMailAddress.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalMailAddress, "tfLegalMailAddress");
		*/
		
		tfLegalNumberTelephone = new TextField();
		tfLegalNumberTelephone.addStyleName("tfLegalNumberTelephone");
		valTfLegalNumberTelephone = new Label("",Label.CONTENT_XHTML);
		valTfLegalNumberTelephone.addStyleName("valTfLegalNumberTelephone");
		valTfLegalNumberTelephone.addStyleName("validator");
		valTfLegalNumberTelephone.setVisible(true);
		culLegal.addComponent(valTfLegalNumberTelephone, "valTfLegalNumberTelephone");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalNumberTelephone, valTfLegalNumberTelephone);
		tfLegalNumberTelephone.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalNumberTelephone, "tfLegalNumberTelephone");
		
		tfLegalEmail = new TextField();
		tfLegalEmail.addStyleName("tfLegalEmail");
		valTfLegalEmail = new Label("",Label.CONTENT_XHTML);
		valTfLegalEmail.addStyleName("valTfLegalEmail");
		valTfLegalEmail.addStyleName("validator");
		valTfLegalEmail.setVisible(true);
		culLegal.addComponent(valTfLegalEmail, "valTfLegalEmail");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalEmail, valTfLegalEmail);
		tfLegalEmail.addListener(fieldRequiredBlurListener);
		emailValidateBlurListener = new EmailValidateBlurListener(tfLegalEmail, valTfLegalEmail);
		tfLegalEmail.addListener(emailValidateBlurListener);
		culLegal.addComponent(tfLegalEmail, "tfLegalEmail");
		
		tfLegalOtherDoc = new TextField();
		tfLegalOtherDoc.addStyleName("tfLegalOtherDoc");
		tfLegalOtherDoc.setWordwrap(true);
		tfLegalOtherDoc.setRows(2);
		culLegal.addComponent(tfLegalOtherDoc, "tfLegalOtherDoc");
		
		return culLegal;
	}
	
	private CustomLayout buildErrorLayout() {
		culError = new CustomLayout("error");
		culError.addStyleName("culError");
		return culError;
	}
	
	private CustomLayout buildSentLayout() {
		culSent = new CustomLayout("sent");
		culSent.addStyleName("culSent");
		return culSent;
	}
	
}
