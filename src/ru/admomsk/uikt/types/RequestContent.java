package ru.admomsk.uikt.types;

public class RequestContent {
	private String purposeRequest = "";
	private String requestedDocument = "";
	private String scopeDocument = "";
	private String nameDocument = "";
	private String numberDocument = "";
	private String publicationDate = "";
	private String otherInformation = "";
	private String type = "";
	
	public void setPurposeRequest(String purposeRequest) {
		this.purposeRequest = purposeRequest;
	}
	public String getPurposeRequest() {
		return purposeRequest;
	}
	
	public void setRequestedDocument(String requestedDocument) {
		this.requestedDocument = requestedDocument;
	}
	public String getRequestedDocument() {
		return requestedDocument;
	}
	
	public void setScopeDocument(String scopeDocument) {
		this.scopeDocument = scopeDocument;
	}
	public String getScopeDocument() {
		return scopeDocument;
	}
	
	public void setNameDocument(String nameDocument) {
		this.nameDocument = nameDocument;
	}
	public String getNameDocument() {
		return nameDocument;
	}
	
	public void setNumberDocument(String numberDocument) {
		this.numberDocument = numberDocument;
	}
	public String getNumberDocument() {
		return numberDocument;
	}
	
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	public String getPublicationDate() {
		return publicationDate;
	}
	
	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}
	public String getOtherInformation() {
		return otherInformation;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	
	
}
