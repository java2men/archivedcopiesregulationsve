package ru.admomsk.uikt.utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	private String day;
	private String mon;
	private String yea;
	private Date date;
	
	public DateUtils(Date date) {
		this.date = date;
		Locale local = new Locale("ru","RU");
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, local);
		String dateFormat = dateFormatter.format(date);
		day = dateFormat.substring(0, dateFormat.indexOf("."));
		mon = dateFormat.substring(dateFormat.indexOf(".")+1, dateFormat.lastIndexOf("."));
		yea = dateFormat.substring(dateFormat.lastIndexOf(".")+1);
	}
	
	public String getStringDay(){
		return day;
	}
	
	public String getStringMonth(){
		return mon;
	}
	
	public String getStringYear(){
		return yea;
	}
	
	public int getIntDay(){
		return Integer.valueOf(day);
	}
	
	public int getIntMonth(){
		return Integer.valueOf(mon);
	}
	
	public int getIntYear(){
		return Integer.valueOf(yea);
	}
	
	
	static public boolean isCorrect(Object date) {
		if (date instanceof Date) return true;
		return false;
	}
}
