package ru.admomsk.uikt.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import ru.admomsk.uikt.types.Individual;
import ru.admomsk.uikt.types.Legal;
import ru.admomsk.uikt.types.RequestContent;
import ru.admomsk.uikt.utils.SendMailUsage;
import ru.admomsk.uikt.view.View;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2.PortletListener;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ArchCopRegApplication extends Application {
	private static boolean reCreateView = false;
	private View view;
	
	@Override
	public void init() {
		createView();
	}
	
	private void createView () {
		view = new View();
		setTheme("archivedcopregtheme");
		//Window mainWindow = new Window();
		//setMainWindow(mainWindow);
		
		//final SessionGuard sessionGuard = new SessionGuard();
		//sessionGuard.setTimeoutWarningPeriod(1);
		//sessionGuard.setKeepalive(true);
		//sessionGuard.setImmediate(true);
		
		//mainWindow.addComponent(sessionGuard);
		//mainWindow.addComponent(view);
		
		//final SessionGuard sessionGuard = new SessionGuard();
		//sessionGuard.setKeepalive(true);
		//sessionGuard.setImmediate(true);
		//view.addComponent(sessionGuard);
		setMainWindow(view);
		
		
		//PortletApplicationContext2 ctx1 = (PortletApplicationContext2)getMainWindow().getApplication().getContext();
		
		
		if (getContext() instanceof PortletApplicationContext2) {
			PortletApplicationContext2 ctx =
				(PortletApplicationContext2) getContext();
			//PortletSession session = (PortletSession)ctx.getPortletSession();
			//session.setMaxInactiveInterval(1);
			ctx.addPortletListener(this, new PortletListener() {
				
				public void handleResourceRequest(ResourceRequest request,
						ResourceResponse response, Window window) {
					// TODO Auto-generated method stub
					/*PortletSession portletSession = request.getPortletSession();
					Map map = portletSession.getAttributeMap();
					Set setKeys = map.keySet();
					Object[] keys = setKeys.toArray();
					Collection<Object> collValues = map.values();
					Object[] values = collValues.toArray();
					System.out.println("keys.length="+keys.length);
					for (int i=0; i<keys.length; i++) {
						
						System.out.println("key"+i+"="+keys[i]+" "+"values"+i+"="+values[i]);
					}
					
					System.out.println("portletSession.getMaxInactiveInterval()="+portletSession.getMaxInactiveInterval());
					//portletSession.setMaxInactiveInterval(1);
					System.out.println("portletSession.getMaxInactiveInterval()="+portletSession.getMaxInactiveInterval());
					System.out.println("portletSession.getLastAccessedTime()="+portletSession.getLastAccessedTime());
					System.out.println("portletSession.getCreationTime()="+portletSession.getCreationTime());*/
				}
				
				public void handleRenderRequest(RenderRequest request,
						RenderResponse response, Window window) {
					
					//Если reCreateView == true, то пересоздать
					if (reCreateView) {
						view = new View();
						//createView();
						setMainWindow(view);
						//reCreateView = true;
						//System.out.println("recreate view");
					}
					PortletSession portletSession = request.getPortletSession();
					/*Map map = portletSession.getAttributeMap();
					Set setKeys = map.keySet();
					Object[] keys = setKeys.toArray();
					Collection<Object> collValues = map.values();
					Object[] values = collValues.toArray();
					//System.out.println("keys.length="+keys.length);
					for (int i=0; i<keys.length; i++) {
						
						System.out.println("key"+i+"="+keys[i]+" "+"values"+i+"="+values[i]);
					}*/
					/*
					System.out.println("portletSession.getMaxInactiveInterval()="+portletSession.getMaxInactiveInterval());
					//portletSession.setMaxInactiveInterval(1);
					System.out.println("portletSession.getMaxInactiveInterval()="+portletSession.getMaxInactiveInterval());
					System.out.println("portletSession.getLastAccessedTime()="+portletSession.getLastAccessedTime());
					System.out.println("portletSession.getCreationTime()="+portletSession.getCreationTime());
					*/
					view.setRefreshIntervalSession(portletSession.getMaxInactiveInterval()-60);
					//view.setRefreshIntervalSession(60*3);
					view.startRefreshSession();
					//System.out.println("view.getRefreshIntervalSession()="+view.getRefreshIntervalSession());
					//Сделать возможность пересоздавать view в будущем
					reCreateView = true;
				}
				
				public void handleEventRequest(EventRequest request,
						EventResponse response, Window window) {
					// TODO Auto-generated method stub
				}
				
				public void handleActionRequest(ActionRequest request,
						ActionResponse response, Window window) {
					// TODO Auto-generated method stub
				}
			});
		}
	}
	
	public static void reCreateView() {
		reCreateView = true;
	}
	
	@SuppressWarnings("finally")
	public static boolean sendMessage (RequestContent requestContent, Individual individual, Legal legal) {
		
		//request.xml (создать временно)
		File request = null;
		File requestDir = null;
		ArrayList<File> attach = null;
		boolean result = false;
		try {
			SendMailUsage sendMailUsage = new SendMailUsage();
			//sendMailUsage.setFrom("ilya.false@gmail.com");
			//sendMailUsage.setFrom("ialozhnikov@admomsk.ru");
			sendMailUsage.setFrom("noreply@admomsk.ru");
			//sendMailUsage.setTo("ilya.lozhnikov13@gmail.com");
			//sendMailUsage.setTo("ialozhnikov@admomsk.ru");
			sendMailUsage.setTo("coosedd@admomsk.ru");
			//стандартный
			//sendMailUsage.setHost("smtp.gmail.com");
			//localhost gmail
			//sendMailUsage.setHost("10.0.0.6");
			//с пробного сервака gmail?
			//sendMailUsage.setHost("217.25.215.18");
			//localhost admomsk
			//sendMailUsage.setHost("10.0.0.29");
			//с портала admomsk
			sendMailUsage.setHost("217.25.215.29");
			//ошибка
			//sendMailUsage.setHost("1.1.1.1");
			
			//sendMailUsage.setUsername("ilya.false");
			//sendMailUsage.setUsername("noreply");
			sendMailUsage.setUsername("Portal");
			//sendMailUsage.setPassword("ialoz");
			sendMailUsage.setSubject("1234");
			//sendMailUsage.setDate(Calendar.getInstance().getTime());
			sendMailUsage.setText("");
			
			//Создать request.xml
			PrintWriter pw = null;
			
			File tempFile = File.createTempFile("archived_copies_regulations_ve_request", "");
    		//Получить path временного файла
    		String pathTempFile = tempFile.getAbsolutePath();
    		//Удалить временный файл
    		org.apache.commons.io.FileUtils.forceDelete(tempFile);
    		//создать директорию на основе path временного файла
    		requestDir = new File(pathTempFile);
    		org.apache.commons.io.FileUtils.forceMkdir(requestDir);	 
			
			//создать временную папку для request.xml
			/*requestDir = File.createTempFile("request", "");
			requestDir.delete();
			requestDir.mkdir();
			if (!requestDir.isDirectory()) return false;*/
			
			//создать request.xml
			request = new File(requestDir, "request.xml");
			
			try {
				pw = new PrintWriter(new FileOutputStream(request));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			/*Записать данные request xml*/
			
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			pw.println("<ArchivedCopies xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\">");
			pw.println("<applicantType>"+requestContent.getType()+"</applicantType>");
			pw.println("<login>"+individual.getEmail()+"</login>");
			pw.println("<nameLast>"+individual.getLastName()+"</nameLast>");
			pw.println("<nameFirst>"+individual.getFirstName()+"</nameFirst>");
			pw.println("<nameMiddle>"+individual.getMiddleName()+"</nameMiddle>");
			pw.println("<alterName>"+individual.getChangeLastName()+"</alterName>"); 
			//проверка на пустоту
			if (requestContent.getType().equals("individual")) {
			pw.println("<passport>"+
					individual.getSeriesPassport()+"; "+
					individual.getNumberPassport()+"; "+
					individual.getWhoGivePassport()+"; "+
					individual.getDateIssuePassport()
					+"</passport>");
			} else pw.println("<passport></passport>");
			pw.println("<phone>"+individual.getNumberTelephone()+"</phone>");
			pw.println("<country>"+individual.getCountry()+"</country>");
			pw.println("<state></state>");
			pw.println("<region>"+individual.getRegion()+"</region>"); 
			pw.println("<city>"+individual.getCity()+"</city>");
			pw.println("<locality></locality>");
			pw.println("<street>"+individual.getStreet()+"</street>");
			pw.println("<house>"+individual.getHome()+"</house>");
			pw.println("<corp>"+individual.getHousing()+"</corp>");
			pw.println("<flat>"+individual.getApartment()+"</flat>");
			
			if (individual.getSupportDoc() != null) {
				for (int i=0; i<individual.getSupportDoc().size(); i++){			
					pw.println("<supportDocument>"+individual.getSupportDoc().get(i).toString()+"</supportDocument>");
				}
				//если нет документов, то записать пустой нод
				if (individual.getSupportDoc().size() == 0) pw.println("<supportDocument></supportDocument>"); 
			} else {
				pw.println("<supportDocument></supportDocument>");
			}
			
			pw.println("<zipCode></zipCode>");
			pw.println("<section></section>");
			pw.println("<room></room>");
			pw.println("<orgName>"+legal.getFullName()+"</orgName>");
			pw.println("<orgOgrn>"+legal.getBIN()+"</orgOgrn>");
			pw.println("<orgInn>"+legal.getINN()+"</orgInn>");
			pw.println("<orgDirectorLastName>"+legal.getLastName()+"</orgDirectorLastName>");
			pw.println("<orgDirectorLastFirst>"+legal.getFirstName()+"</orgDirectorLastFirst>");
			pw.println("<orgDirectorLastMiddle>"+legal.getMiddleName()+"</orgDirectorLastMiddle>");
			pw.println("<orgDirectorPost>"+legal.getPostHead()+"</orgDirectorPost>");
			
			//проверка на пустоту
			String legalAddress = "";
			//если выбрано юр. лицо
			if (requestContent.getType().equals("legal")) {
				legalAddress = legal.getCountry() + ", ";
				if (!legal.getRegion().equals("")) {
					legalAddress = legalAddress + legal.getRegion() + ", ";
				}
				legalAddress = legalAddress + legal.getCity() + ", ";
				legalAddress = legalAddress + legal.getStreet() + " ";
				legalAddress = legalAddress + legal.getHome();
				if (!legal.getHousing().equals("")) {
					legalAddress = legalAddress + ", корп. "+ legal.getHousing();
				}			
				if (!legal.getApartment().equals("")) {
					legalAddress = legalAddress + ", кв. "+ legal.getApartment();
				}
			}
			pw.println("<orgAddress>"+legalAddress+"</orgAddress>");
			
			//проверка на пустоту
			/*
			if (!legal.getCountry().equals("")) {
				pw.println("<orgAddress>"
						+legal.getCountry()+" "
						+legal.getRegion()+" "
						+legal.getCity()+" "
						+legal.getStreet()+" "
						+legal.getHome()+" "
						+legal.getHousing()+" "
						+legal.getApartment()
					+"</orgAddress>");
			} else pw.println("<orgAddress></orgAddress>");
			*/
			//pw.println("<orgAddress>"+legal.getMailAddress()+"</orgAddress>");
			pw.println("<orgTelefon>"+legal.getNumberTelephone()+"</orgTelefon>");
			pw.println("<orgEmail>"+legal.getEmail()+"</orgEmail>");
			
			pw.println("<supportDocument>"+legal.getSupportDoc()+"</supportDocument>");
			
			pw.println("<purposeRequest>"+requestContent.getPurposeRequest()+"</purposeRequest>");
			String requestedDocument = "";
			if (requestContent.getRequestedDocument().equals("копия постановления, " +
					"распоряжения Администрации города Омска (Мэра города Омска) сроком " +
					"издания не ранее 2006 года")) requestedDocument = "1";
			if (requestContent.getRequestedDocument().equals("копия ранее принятого " +
					"распоряжения департамента, затрагивающего " +
					"интересы граждан")) requestedDocument = "2";
			if (requestContent.getRequestedDocument().equals("копия архивного документа, " +
					"подтверждающего право на владение землей")) requestedDocument = "3";
			pw.println("<requestedDocument>"+requestedDocument+"</requestedDocument>");
			String scopeDocument = "";
			if (!requestContent.getScopeDocument().equals("")) {
				if (requestContent.getScopeDocument().equals("имущественные отношения")) scopeDocument = "a";
				if (requestContent.getScopeDocument().equals("образование")) scopeDocument = "b";
				if (requestContent.getScopeDocument().equals("архитектура и градостроительство")) scopeDocument = "c";
			}
			pw.println("<docScope>"+scopeDocument+"</docScope>");
			pw.println("<docName>"+requestContent.getNameDocument()+"</docName>");
			pw.println("<docNumber>"+requestContent.getNumberDocument()+"</docNumber>");
			pw.println("<docDate>"+requestContent.getPublicationDate()+"</docDate>");
			pw.println("<docOther>"+requestContent.getOtherInformation()+"</docOther>");
			pw.println("<depCode>61</depCode>");
			pw.println("<depCodeSub>72</depCodeSub>");
			pw.println("<file></file>");
			pw.println("</ArchivedCopies>");
			
			pw.close();
			
			//Прикрепить request.xml
			attach = new ArrayList<File>();
			attach.add(request);
			sendMailUsage.setFileAsAttachment(attach);
			
			//Послать email
			result = sendMailUsage.sendMessage();
		
		} catch (Exception e) {
			System.out.println("---error sending mail---");
			result = false;
			e.printStackTrace();
			//удалить файлы
			/*for (File a:attach) {
				System.out.println("remove "+a.getName());
				a.delete();
			}
			System.out.println("remove "+requestDir.getName());
			requestDir.delete();*/
		} finally {
			//удалить файлы
			for (File a:attach) {
				//System.out.println("remove "+a.getName());
				try {
					
					if (a.getParentFile() != null) {
						if (a.getParentFile().exists()) {
							org.apache.commons.io.FileUtils.deleteDirectory(a.getParentFile());
						}
					}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			return result;
		}
	}

}
